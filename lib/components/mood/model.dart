class Post {
  final String id;
  final String content;
  final String date;
  final String userid;
  final String type;

  Post(this.id, this.content, this.date, this.userid, this.type);

  Post.fromJson(Map json)
      : id = json['id'],
        content = json['content'],
        date = json['date'],
        type = json['type'],
        userid = json['userid'];

  Map toJson() => {'content': content, 'date': date, 'type': type};
}
