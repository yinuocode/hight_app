class Post {
  final String id;
  final String content;
  final String date;
  final String userid;

  Post(this.id, this.content, this.date, this.userid);

  Post.fromJson(Map json)
      : id = json['id'],
        content = json['content'],
        date = json['date'],
        userid = json['userid'];

  Map toJson() => {'content': content, 'date': date};
}
