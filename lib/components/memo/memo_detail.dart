import 'dart:convert';
import 'dart:io';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:http/http.dart' as http;
import 'dart:async';
import 'package:shared_preferences/shared_preferences.dart';
import '../model.dart';
import 'model.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:keyboard_avoider/keyboard_avoider.dart';

var gItem = {'id': '', 'content': ''};

class MemoDetail extends StatelessWidget {
  final Post item;
  const MemoDetail({Key key, this.item}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    if (item != null) {
      gItem['content'] = item.content;
      gItem['id'] = item.id;
    }
    return MemoDetailForm();
  }
}

class MemoDetailForm extends StatefulWidget {
  @override
  MemoDetailFormState createState() => MemoDetailFormState();
}

class MemoDetailFormState extends State<MemoDetailForm> {
  TextEditingController _content = TextEditingController();
  final memoFormKey = GlobalKey<FormState>();
  bool autovalidate = false;
  void submitMemoForm() {
    if (memoFormKey.currentState.validate()) {
      memoFormKey.currentState.save();
      this.fetchPosts(this._content.text);
      EasyLoading.show(
          status: 'loading...', maskType: EasyLoadingMaskType.black);
    } else {
      setState(() {
        autovalidate = true;
      });
    }
  }

  @override
  void initState() {
    super.initState();
    this._content.text = gItem['content']; // 设置初始值
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
        centerTitle: true,
        title: Text(gItem['id'] != '' ? '编辑便签' : '新增便签'),
        actions: <Widget>[
          ElevatedButton(
            style: ButtonStyle(
                // foregroundColor: Theme.of(context).accentColor, //字体颜色
                elevation: MaterialStateProperty.all(0)),
            child: Text('提交', style: TextStyle(color: Colors.white)),
            onPressed: this.submitMemoForm,
          ),
        ],
      ),
      body: Theme(
        data: Theme.of(context).copyWith(
          primaryColor: Colors.black,
        ),
        child: Container(
            decoration: new BoxDecoration(
              color: Colors.blue[50],
            ),
            child: KeyboardAvoider(
              autoScroll: true,
              child: Column(
                  // mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Form(
                        key: memoFormKey,
                        child: Container(
                          child: new ConstrainedBox(
                            constraints:
                                BoxConstraints(maxHeight: double.infinity),
                            child: TextField(
                              keyboardType: TextInputType.multiline,
                              strutStyle: StrutStyle(height: 1.86),
                              minLines: 15,
                              maxLines: null,
                              decoration: InputDecoration(
                                  contentPadding: EdgeInsets.all(16.0),
                                  fillColor: Colors.blue[50],
                                  filled: true,
                                  isCollapsed: true,
                                  // labelText: '请输入便签内容',
                                  // helperText: '请输入便签内容',
                                  border: InputBorder.none),
                              controller: _content,
                              onSubmitted: (value) {
                                this.setState(() {
                                  this._content.text = value;
                                });
                              },
                            ),
                          ),
                        ))
                  ]),
            )),
      ),
    );
  }

  Future fetchPosts($content) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var dataBody = jsonEncode(
        {'id': gItem['id'] != '' ? gItem['id'] : '', 'content': $content});
    print(dataBody);
    var response;
    // 如果内容为空
    if ($content == '') {
      // 并且是新增 则不做任何处理
      if (gItem['id'] == '') {
        Navigator.pop(context);
        return false;
      } else {
        // 并且是编辑 则调用删除的接口
        response = await http.get(
            'https://me.yuanqiao.pw/api/me/deleteRecord.php?id=' + gItem['id'],
            headers: {
              HttpHeaders.authorizationHeader: prefs.get('user_token')
            });
      }
    } else {
      // 如果内容不为空
      response = await http.post(
          'https://me.yuanqiao.pw/api/me/insertRecord.php',
          body: dataBody,
          headers: {HttpHeaders.authorizationHeader: prefs.get('user_token')});
    }
    EasyLoading.dismiss();
    if (response.statusCode == 200) {
      gItem['id'] = '';
      gItem['content'] = '';
      final responseBody = json.decode(response.body);
      print('responseBody');
      print(responseBody);
      Fluttertoast.showToast(
          msg: responseBody['error'],
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.CENTER,
          timeInSecForIosWeb: 1,
          backgroundColor: Colors.white,
          textColor: Colors.black87,
          fontSize: 16.0);
      Navigator.of(context).pushNamed('/home', arguments: ScreenArguments(1));
    } else {
      Fluttertoast.showToast(
          msg: '网络或服务器错误',
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.CENTER,
          timeInSecForIosWeb: 1,
          backgroundColor: Colors.white,
          textColor: Colors.black87,
          fontSize: 16.0);
    }
  }
}
