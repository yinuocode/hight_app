import 'dart:convert';
import 'dart:io';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:http/http.dart' as http;
import 'dart:async';
import 'package:shared_preferences/shared_preferences.dart';
import 'model.dart';

class PropertyLog extends StatelessWidget {
  final String id;
  PropertyLog({Key key, this.id}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Container(
        padding: EdgeInsets.only(top: 35),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Title(color: Colors.black, child: Text('修改日志 ')),
            Container(child: PropertyLogList(id: id))
          ],
        ));
  }
}

class PropertyLogList extends StatefulWidget {
  final String id;
  PropertyLogList({Key key, this.id}) : super(key: key);
  @override
  _PropertyLogListState createState() => _PropertyLogListState(id: id);
}

class _PropertyLogListState extends State<PropertyLogList> {
  String id;
  _PropertyLogListState({Key key, this.id});
  String totalSum;
  @override
  void initState() {
    super.initState();
  }

  Future fetchPosts() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    // 获得密码
    final codeResponse = await http.get(
      'https://me.yuanqiao.pw/api/me/getPropertyLog.php?id=' + id,
      headers: {HttpHeaders.authorizationHeader: prefs.get('user_token')},
    );
    if (codeResponse.statusCode == 200) {
      final codeResponseBody = json.decode(codeResponse.body);
      if (codeResponseBody['state'] == '3') {
        Navigator.of(context).pushNamedAndRemoveUntil(
          '/login',
          (route) => route == null,
        );
      } else {
        totalSum = codeResponseBody['total'];
        List<PostLog> posts = codeResponseBody['data']
            .map<PostLog>((item) => PostLog.fromJson(item))
            .toList();
        return posts;
      }
    } else {
      Fluttertoast.showToast(
          msg: '网络或服务器错误',
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.CENTER,
          timeInSecForIosWeb: 1,
          backgroundColor: Colors.white,
          textColor: Colors.black87,
          fontSize: 16.0);
    }
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: fetchPosts(),
      builder: (BuildContext context, AsyncSnapshot snapshot) {
        print('data: ${snapshot.data}');
        print('connectionState: ${snapshot.connectionState}');
        if (snapshot.connectionState == ConnectionState.waiting) {
          return Center(
            child: Padding(
              padding: EdgeInsets.all(10.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Text(
                    '加载中',
                    style: TextStyle(fontSize: 16.0),
                  ),
                  SizedBox(
                    child: CircularProgressIndicator(strokeWidth: 1.0),
                    height: 20.0,
                    width: 20.0,
                  )
                  // 加载图标
                ],
              ),
            ),
          );
        } else {
          if (snapshot.data.length == 0) {
            return Center(
                child: Container(
              padding: EdgeInsets.only(bottom: 35),
              child: Text('还没有修改记录', style: TextStyle(color: Colors.black38)),
            ));
          }
          // return Container(child: Center(child: Text('fsdfdsfs')));
        }
        // snapshot.data.unshift({});
        return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: snapshot.data.map<Widget>((item) {
              return Text(
                  item.modify_user +
                      ' 在 ' +
                      item.modify_time +
                      ' 将金额 ' +
                      item.old_money +
                      ' 改为 ' +
                      item.new_money,
                  style: TextStyle(
                      fontSize: 14, height: 2, color: Colors.grey[700]));
            }).toList());
      },
    );
  }
}
