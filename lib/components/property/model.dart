class Post {
  final String id;
  final String account_type;
  final String account_name;
  final String money;
  final String due_date;
  final String is_statistic;
  final String userid;

  Post(this.id, this.account_type, this.account_name, this.money, this.due_date,
      this.is_statistic, this.userid);

  Post.fromJson(Map json)
      : id = json['id'],
        account_type = json['account_type'],
        account_name = json['account_name'],
        money = json['money'],
        due_date = json['due_date'],
        is_statistic = json['is_statistic'],
        userid = json['userid'];
  Map toJson() => {
        'account_type': account_type,
        'account_name': account_name,
        'money': money,
        'due_date': due_date,
        'is_statistic': is_statistic
      };
}

class PostLog {
  final String id;
  final String property_id;
  final String new_money;
  final String old_money;
  final String account_name;
  final String modify_user;
  final String modify_time;
  final String userid;

  PostLog(this.id, this.property_id, this.new_money, this.old_money,
      this.account_name, this.modify_user, this.modify_time, this.userid);

  PostLog.fromJson(Map json)
      : id = json['id'],
        property_id = json['property_id'],
        new_money = json['new_money'],
        old_money = json['old_money'],
        account_name = json['account_name'],
        modify_user = json['modify_user'],
        modify_time = json['modify_time'],
        userid = json['userid'];
  Map toJson() => {
        'property_id': property_id,
        'new_money': new_money,
        'old_money': old_money,
        'account_name': account_name,
        'modify_user': modify_user,
        'modify_time': modify_time,
      };
}
