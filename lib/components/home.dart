import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'model.dart';
import 'code/code_book.dart';
import 'memo/memo_list.dart';
import 'mood/mood_list.dart';
import 'property/property_home.dart';
import 'package:fluttertoast/fluttertoast.dart';
// import 'mood/mood_list.dart';
// import 'task/task_list.dart';

class Home extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _HomeState();
  }
}

class _HomeState extends State<Home> {
  DateTime _lastTime;
  // int index;
  // _HomeState({Key key, @required this.index});
  final _bottomNavigationColor = Colors.grey;
  final _bottomNavigationSelectColor = Colors.blue;
  int _currentIndex = 0;
  var _controller = PageController(
    initialPage: 0,
  );
  bool isArgs = false;
  void initState() {
    super.initState();
    _validateLogin();
    this.isArgs = true;
  }

  @override
  void dispose() {
    super.dispose();
    _controller.dispose();
  }

  @override
  Widget build(BuildContext context) {
    if (isArgs == true) {
      setState(() {
        isArgs = false;
      });
      ScreenArguments args = ModalRoute.of(context).settings.arguments;
      if (args != null) {
        setState(() {
          _currentIndex = args.index;
        });
        _controller = PageController(
          initialPage: args.index,
        );
      }
    }

    //获取传递的参数
    return WillPopScope(
        child: Scaffold(
          body: PageView(
            controller: _controller,
            children: <Widget>[
              CodeBook(),
              MemoList(),
              PropertyList(),
              // TaskList(),
              MoodList(),
            ],
            physics: NeverScrollableScrollPhysics(),
          ),
          bottomNavigationBar: BottomNavigationBar(
            currentIndex: _currentIndex,
            onTap: (index) {
              _controller.jumpToPage(index);
              setState(() {
                _currentIndex = index;
              });
            },
            // onTap: _onTapHandler,
            type: BottomNavigationBarType.fixed,
            fixedColor: Colors.blue,
            items: [
              BottomNavigationBarItem(
                icon: Icon(Icons.lock),
                // ignore: deprecated_member_use
                title: Text('密码'),
              ),
              BottomNavigationBarItem(
                icon: Icon(Icons.description),
                // ignore: deprecated_member_use
                title: Text('便签'),
              ),
              BottomNavigationBarItem(
                icon: Icon(Icons.monetization_on),
                // ignore: deprecated_member_use
                title: Text('资产'),
              ),
              BottomNavigationBarItem(
                icon: Icon(Icons.favorite),
                // ignore: deprecated_member_use
                title: Text('心情'),
              ),
            ],
          ),
        ),
        onWillPop: () async {
          if (_lastTime == null ||
              DateTime.now().difference(_lastTime) > Duration(seconds: 1)) {
            //两次点击间隔超过1s重新计时
            _lastTime = DateTime.now();
            Fluttertoast.showToast(
                msg: '再按一次退出应用',
                toastLength: Toast.LENGTH_SHORT,
                gravity: ToastGravity.CENTER,
                timeInSecForIosWeb: 1,
                backgroundColor: Colors.white,
                textColor: Colors.black87,
                fontSize: 16.0);
            return false;
          }
          return true;
        });
  }

  Future _validateLogin() async {
    Future<dynamic> future = Future(() async {
      SharedPreferences prefs = await SharedPreferences.getInstance();
      return prefs.getString("user_token");
    });
    future.then((val) {
      if (val == null) {
        Navigator.of(context).pushNamed('/login');
      }
    }).catchError((_) {
      print("catchError");
    });
  }
}
