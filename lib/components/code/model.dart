class Post {
  final String id;
  final String website;
  final String date;
  final String email;
  final String username;
  final String weblink;
  final String userid;
  final String password;
  final String passwordText;
  final String remark;

  Post(this.id, this.website, this.date, this.email, this.username,
      this.weblink, this.userid, this.password, this.passwordText, this.remark);

  Post.fromJson(Map json)
      : id = json['id'],
        website = json['website'],
        date = json['date'],
        email = json['email'],
        username = json['username'],
        weblink = json['weblink'],
        userid = json['userid'],
        password = json['password'],
        passwordText = json['passwordText'],
        remark = json['remark'];

  Map toJson() => {
        'username': username,
        'website': website,
      };
}
