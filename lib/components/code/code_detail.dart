import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:shared_preferences/shared_preferences.dart';
import './model.dart';
import 'add_code.dart';
import 'package:http/http.dart' as http;

//创建商品详情页视图
class MeCodeDetail extends StatelessWidget {
  final Post item;
  //初始化函数
  MeCodeDetail({Key key, @required this.item}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("密码详情"),
      ),
      body: Center(
        //这里从前一视图传递了参数进来，没有有任何的修改
        child: GoodsShowCloumn(
          item: this.item,
        ),
      ),
    );
  }
}

class GoodsShowCloumn extends StatefulWidget {
  final Post item;
  //初始化函数
  GoodsShowCloumn({Key key, @required this.item}) : super(key: key);
  @override
  _GoodsShowCloumn createState() => _GoodsShowCloumn(item: this.item);
}

//商品详细展示的垂直布局类
class _GoodsShowCloumn extends State<GoodsShowCloumn> {
  Post item;
  //初始化函数
  _GoodsShowCloumn({Key key, @required this.item});
  // 创建内置属性接受外部数据,我也不知道为什么要加final关键词，按照课程还是加上
  // 删除
  Future _deleteItem() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    print('prefs');
    // 获得密码
    final response = await http.get(
      'https://me.yuanqiao.pw/api/me/deleteAccount.php?id=' + item.id,
      headers: {HttpHeaders.authorizationHeader: prefs.get('user_token')},
    );
    print('response');
    print(response);
    if (response.statusCode == 200) {
      final responseBody = json.decode(response.body);
      Fluttertoast.showToast(
          msg: responseBody['error'],
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.CENTER,
          timeInSecForIosWeb: 1,
          backgroundColor: Colors.white,
          textColor: Colors.black87,
          fontSize: 16.0);
      if (responseBody['state'] == '1') {
        Navigator.of(context).pushNamedAndRemoveUntil(
          '/home',
          (route) => route == null,
        );
      }
    } else {
      Fluttertoast.showToast(
          msg: '网络或服务器错误',
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.CENTER,
          timeInSecForIosWeb: 1,
          backgroundColor: Colors.white,
          textColor: Colors.black87,
          fontSize: 16.0);
    }
  }

  _openAlertDialog() {
    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('删除账户'),
          content: Text('确定要删除该条数据吗?'),
          actions: <Widget>[
            TextButton(
              child: Text('取消'),
              onPressed: () {
                Navigator.pop(context);
              },
            ),
            ElevatedButton(
              style: ButtonStyle(
                backgroundColor:
                    MaterialStateProperty.all(Color(0xfff44336)), //背景颜色
              ),
              child: Text('确定'),
              onPressed: () {
                _deleteItem();
                Navigator.pop(context);
              },
            ),
          ],
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return ListView(
      padding: EdgeInsets.all(16),
      children: <Widget>[
        Container(
            padding: EdgeInsets.only(top: 20, bottom: 10),
            child: Row(
                // mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Container(width: 70, child: Text('网站：', style: TextStyle(height: 1.9))),
                  Expanded(
                      child: SelectableText("${item.website}",
                          style: new TextStyle(
                              fontSize: 18.0, color: Colors.black87,height: 1.6))),
                ])),
        Container(
            padding: EdgeInsets.only(top: 10, bottom: 10),
            child: Row(
                // mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Container(width: 70, child: Text('网页链接：', style: TextStyle(height: 1.9))),
                  Expanded(
                      child: SelectableText(
                          "${item.weblink}" != '' ? "${item.weblink}" : '暂无',
                          style: new TextStyle(
                              fontSize: 18.0, color: Colors.black87,height: 1.6))),
                ])),
        Container(
            padding: EdgeInsets.only(top: 10, bottom: 10),
            child: Row(
                // mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Container(width: 70, child: Text('用户名：', style: TextStyle(height: 1.9))),
                  Expanded(
                      child: SelectableText("${item.username}",
                          style: new TextStyle(
                              fontSize: 18.0, color: Colors.black87,height: 1.6))),
                ])),
        Container(
            padding: EdgeInsets.only(top: 10, bottom: 10),
            child: Row(
                // mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Container(width: 70, child: Text('密码提示：', style: TextStyle(height: 1.9))),
                  Expanded(
                      child: SelectableText(
                          "${item.passwordText}" != ''
                              ? "${item.passwordText}"
                              : '暂无',
                          maxLines: null,
                          style: new TextStyle(
                              fontSize: 18.0, color: Colors.black87,height: 1.6))),
                ])),
        Container(
            padding: EdgeInsets.only(top: 10, bottom: 10),
            child: Row(
                // mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Container(width: 70, child: Text('密码：', style: TextStyle(height: 1.9))),
                  Expanded(
                      child: SelectableText(
                          "${item.password}" != '' ? "${item.password}" : '暂无',
                          style: new TextStyle(
                              fontSize: 18.0, color: Colors.black87,height: 1.6))),
                ])),
        Container(
            padding: EdgeInsets.only(top: 10, bottom: 10),
            child: Row(
                // mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Container(width: 70, child: Text('邮箱：', style: TextStyle(height: 1.9))),
                  Expanded(
                      child: SelectableText(
                          "${item.email}" != '' ? "${item.email}" : '暂无',
                          style: new TextStyle(
                              fontSize: 18.0, color: Colors.black87,height: 1.6))),
                ])),
        Container(
            padding: EdgeInsets.only(top: 10, bottom: 10),
            child: Row(
                // mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Container(width: 70, child: Text('备注：', style: TextStyle(height: 1.9))),
                  Expanded(
                      child: SelectableText(
                          "${item.remark}" != '' ? "${item.remark}" : '暂无',
                          maxLines: null,
                          style: new TextStyle(
                              fontSize: 18.0, color: Colors.black87,height: 1.6))),
                ])),
        SizedBox(
          height: 30,
        ),
        ElevatedButton(
          child: Text("编辑"),
          onPressed: () {
            Navigator.of(context).push(MaterialPageRoute(
                builder: (context) => AddCode(item: this.item)));
          },
        ),
        SizedBox(
          height: 15,
        ),
        ElevatedButton(
          style: ButtonStyle(
            backgroundColor:
                MaterialStateProperty.all(Color(0xfff44336)), //背景颜色
          ),
          child: Text("删除"),
          onPressed: _openAlertDialog,
        )
      ],
    );
  }
}
