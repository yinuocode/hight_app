import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'edit_password.dart';
import 'edit_personage.dart';
import 'model.dart';

class DrawerUser extends StatefulWidget {
  @override
  _DrawerUser createState() => _DrawerUser();
}

class _DrawerUser extends State<DrawerUser> {
  String nickname = '';
  String email = '';
  getValue() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    setState(() {
      nickname = prefs.get('nickname');
      email = prefs.get('email');
    });
  }

  @override
  void initState() {
    super.initState();
    getValue();
  }

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        padding: EdgeInsets.zero,
        children: <Widget>[
          UserAccountsDrawerHeader(
            accountName: Text(nickname,
                style: TextStyle(
                    fontWeight: FontWeight.bold, color: Colors.white)),
            accountEmail: Text(email),
            currentAccountPicture: CircleAvatar(
              backgroundImage: new ExactAssetImage('images/avatar.png'),
            ),
            decoration: BoxDecoration(
              color: Colors.blue[50],
              image: DecorationImage(
                image: new ExactAssetImage('images/info-bg.jpg'),
                fit: BoxFit.cover,
                colorFilter: ColorFilter.mode(
                    Colors.blue.withOpacity(0.4), BlendMode.hardLight),
              ),
            ),
          ),
          ListTile(
              title: Text(
                '个人信息',
                textAlign: TextAlign.right,
              ),
              trailing: Icon(Icons.person, color: Colors.black45, size: 22.0),
              onTap: () async {
                var data = await Navigator.of(context).push(MaterialPageRoute(
                    builder: (context) =>
                        EditPersonage(userObj: new UserObj(nickname, email))));
                this.getValue();
              }),
          ListTile(
              title: Text(
                '修改密码',
                textAlign: TextAlign.right,
              ),
              trailing: Icon(Icons.edit, color: Colors.black45, size: 22.0),
              onTap: () => Navigator.of(context).push(
                  MaterialPageRoute(builder: (context) => EditPassword()))),
          ListTile(
              title: Text(
                '退出登录',
                textAlign: TextAlign.right,
              ),
              trailing: Icon(Icons.logout, color: Colors.black45, size: 22.0),
              onTap: () async {
                SharedPreferences prefs = await SharedPreferences.getInstance();
                String userName = prefs.get('username');
                prefs.clear(); //清空键值对
                prefs.setString('username', userName);
                Fluttertoast.showToast(
                    msg: '退出成功',
                    toastLength: Toast.LENGTH_SHORT,
                    gravity: ToastGravity.CENTER,
                    timeInSecForIosWeb: 1,
                    backgroundColor: Colors.white,
                    textColor: Colors.black87,
                    fontSize: 16.0);
                Navigator.of(context).pushNamedAndRemoveUntil(
                  '/login',
                  (route) => route == null,
                );
              }),
        ],
      ),
    );
  }
}
