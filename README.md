# flutter 版本密码管理系统


#### 日前开发出来的微信小程序版
<img width="180" src="https://yuanqiao.pw/static/img/qrcode.jpg"/>

#### 介绍
flutter 版本密码管理系统

#### 功能
登录注册
退出登录
修改个人信息
修改密码
密码本增删改查搜索
便签增删改查
app名称和图标
记住用户名P
忘记密码
发送邮箱验证码倒计时
#### 使用
1. 安装包下载

<a href="https://gitee.com/yinuocode/hight_app/raw/master/release-android-apk-file/secret.apk">点击下载安卓apk</a> 或者扫码下载<br>
<img width="180" src="https://gitee.com/yinuocode/hight_app/raw/master/release-android-apk-file/release-apk.png"/>

2. 测试账号密码

账号：12345<br>
密码：12345

3. 软件展示图
<img width="300" src="https://gitee.com/yinuocode/hight_app/raw/master/release-android-apk-file/img/img1.jpg"/>
<img width="300" src="https://gitee.com/yinuocode/hight_app/raw/master/release-android-apk-file/img/img2.png"/>
<img width="300" src="https://gitee.com/yinuocode/hight_app/raw/master/release-android-apk-file/img/img3.png"/>
<img width="300" src="https://gitee.com/yinuocode/hight_app/raw/master/release-android-apk-file/img/img4.png"/>

#### 调试

1. 安装依赖

source ~/.bash_profile
flutter packages get

2. 打包
- 打包成 aab 文件 flutter build appbundle
- 打包成 apk 文件 flutter build apk### flutter 包

3. 安装依赖包

flutter pub add xxx
### 创建应用
 1. vscode中安装flutter扩展，安装后重启vscode
 2. 命令面板中（shift+command+p）输入 doctor 选择 Flutter: Run Flutter Doctor。
 3. 打开 输出 面板查看是否有错误。（有错误根据提示修复）
 4. 命令面板中输入 “flutter”，选择 Flutter: New Project。输入项目名称回车即可。
 5. 点击右下角 No Devices 然后打开一个模拟器。
 6. 选择 调试 > 启动调试 或者按F5。

### 其它
> ios 设备运行报错问题 didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey
在ios/Runner/AppDelegate.swift中
  UIApplication.LaunchOptionsKey=》UIApplicationLaunchOptionsKey就ok了

### flutter 文档地址
https://flutter.cn/docs
https://pub.flutter-io.cn/



flutter doctor --android-licenses